package task7;

class Fib {

    private final int number;
    private final long value;

    Fib(int number) {
        this.number = number;
        value = calculateFib();
    }

    private long calculateFib() {
        //w tym zadaniu potrzebujemy trzech zmiennych i trzech wartości początkowych, ponieważ ostatnia z nich będzie zależeć od dwóch poprzednich
        long previous = 0;
        long current = 1;
        long newFib = 1;

        for (int i = 1; i < number; i++) {
            newFib = current + previous; //nowy wyraz ciągu jest sumą dwóch poprzednich - nic dodać, nic ująć
            previous = current; //obecny wyraz musi ustąpić miejsca i stać się poprzednim...
            current = newFib; //... i nie jest w cale poszkodowany, ponieważ on sam awansuje na wyższe miejsce
        }

        return newFib;
    }

    long getValue() {
        return value;
    }
}
