package task7;

import java.util.Scanner;

class MainTask7 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a positive number: ");
        int number = scanner.nextInt();

        var fib = new Fib(number);
        System.out.println(String.format("Fib: %s", fib.getValue()));
    }
}
