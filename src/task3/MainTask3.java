package task3;

import java.util.Scanner;

class MainTask3 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        int a = scanner.nextInt();

        System.out.print("Enter b: ");
        int b = scanner.nextInt();

        System.out.print("Enter c: ");
        int c = scanner.nextInt();

        var quadraticFunction = new QuadraticFunction(a, b, c);
        XSolution[] XSolutions = quadraticFunction.calculateSolutions();

        if (XSolutions.length == 0) {
            System.out.println("Delta has a negative value");
            return;
        }

        System.out.println("Solutions:");
        for (XSolution XSolution : XSolutions) {
            System.out.println(XSolution);
        }
    }
}
