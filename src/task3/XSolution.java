package task3;

class XSolution {

    private final double value;

    XSolution(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
