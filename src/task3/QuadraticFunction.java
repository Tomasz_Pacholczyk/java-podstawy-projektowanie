package task3;

class QuadraticFunction {

    private final int a;
    private final int b;
    private final int c;

    QuadraticFunction(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    XSolution[] calculateSolutions() {
        int delta = b * b - 4 * a * c;
        System.out.println(String.format("Delta: %s", delta));
        if (delta < 0) {
            return new XSolution[]{};
        } else if (delta == 0) {
            var x0 = new XSolution(calculateX1(delta)); //tutaj moglibyśmu wywołać "calculateX2(delta)" zamiast X1 - nie ma to znaczenia, bo i tak dadzą ten sam rezultat (przecież delta jest 0, więc funkcja ma tylko jeden pierwiastek).
            return new XSolution[]{x0};
        } else {
            var x1 = new XSolution(calculateX1(delta));
            var x2 = new XSolution(calculateX2(delta));
            return new XSolution[]{x1, x2};
        }
    }

    private double calculateX1(int delta) {
        return (-1 * b + Math.sqrt(delta)) / (2 * a);
    }

    private double calculateX2(int delta) {
        return (-1 * b - Math.sqrt(delta)) / (2 * a);
    }
}
