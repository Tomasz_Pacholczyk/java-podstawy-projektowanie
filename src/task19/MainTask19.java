package task19;

class MainTask19 {

    public static void main(String[] args) {
        Poem[] poems = createInitData();
        MaxResult maxResult = null;
        int maxStropheNumber = 0;
        for (Poem poem : poems) {
            int currentPoemStropheNumbers = poem.getStropheNumbers();
            if (currentPoemStropheNumbers > maxStropheNumber) {
                maxStropheNumber = currentPoemStropheNumbers;
                maxResult = new MaxResult(currentPoemStropheNumbers, poem.getCreator());
            }
        }

        System.out.println(String.format("Poem with max strophe numbers: %s", maxResult));
    }

    private static Poem[] createInitData() {
        var authorA = new Author("Author A", "Nationality A");
        var authorB = new Author("Author B", "Nationality B");
        var authorC = new Author("Author C", "Nationality C");

        Poem[] poems = {
                new Poem(authorA, 12),
                new Poem(authorB, 7),
                new Poem(authorC, 120),
                new Poem(authorC, 20),
                new Poem(authorC, 2)
        };
        return poems;
    }
}

