package task19;

class Author {

    private final String surname;
    private final String nationality;

    Author(String surname, String nationality) {
        this.surname = surname;
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return surname;
    }
}
