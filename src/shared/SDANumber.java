package shared;

public class SDANumber {

    private final int value;

    public SDANumber(int value) {
        this.value = value;
    }

    public boolean isPrime() {
        //Liczba pierwsza to taka, która ma dwa dzielniki: "1" i samą siebie
        for (int i = 2; i < value; i++) {
            //Tutaj tylko sprawdzamy, czy ma jeszcze jakiś inny dzielnik
            if (value % i == 0) {
                //Jeśli ma, to wtedy nie jest pierwsza i od razu przerywamy funkcję, bo nie ma sensu sprawdzać dalej (np. czy ma 3 czy 200 dzielników - nadal nie jest pierwsza)
                return false;
            }
        }

        return true;
    }

    public boolean isDivisibleBy(int divisor) {
        return value % divisor == 0;
    }

    public double calculateHarmonicSeries() {
        int i = 1;
        double sum = 0;
        while (i <= value) {
            double element = 1.00 / i;
            sum += element; //w każdej iteracji pętli zwiększamy wartość sumy o element iterowany (musimy mieć do czego dodać przy pierwszym obrocie, więc dlatego na początku jest ona zerem)
            i++;
        }

        return sum;
    }

    public int calculateSumOfDigits() {
        int sum = 0; //nasz finalna suma, wiadomo, na początku 0
        int rest; //to jest nasza poszczególna cyfra w liczbie i ją będziemy dodawać do sumy przy każdym obrocie
        int mainPart = value;

        while (mainPart > 0) {
            rest = mainPart % 10; //idziemy sobie od prawej do lewej z naszymi cyframi
            sum += rest; //...dodajemy...
            mainPart = mainPart / 10; /* To już jest specyfika naszego systemu liczbowego, którym się posługujemy na co dzień (jest to system dziesiętny).
                                         Dla nas jest to oczywiste, bo widzimy okiem te cyfry w liczbie, ale komputer tego nie widzi i tu go właśnie uczymy.
                                         Są jeszcze inne, np. dwójkowy (czyli binarny - dla komputera).
                                         Zachęcam do przerobienia jakiegoś przykładu na kartce papieru i podstawiania sobie tych wartości */
        }

        return sum;
    }
}