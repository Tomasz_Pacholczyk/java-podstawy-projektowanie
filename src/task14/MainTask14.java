package task14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class MainTask14 {

    public static void main(String[] args) throws IOException {
        var bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the first character:  ");
        String firstInput = bufferedReader.readLine();
        if (!isInputCorrect(firstInput)) {
            return;
        }
        char firstCharacter = firstInput.charAt(0);

        System.out.print("Enter the second character:  ");
        String secondInput = bufferedReader.readLine();

        if (!isInputCorrect(secondInput)) {
            return;
        }

        char secondCharacter = secondInput.charAt(0);

        int result = Math.abs(firstCharacter - secondCharacter) - 1;
        System.out.println(String.format("There are %s characters between %s and %s", result, firstCharacter, secondCharacter));
    }

    private static boolean isInputCorrect(String input) {
        if (input.isBlank()) {
            System.out.println("Provided blank character");
            return false;
        }

        if (input.length() > 1) {
            System.out.println("Provided more than one character");
            return false;
        }

        char character = input.charAt(0);

        if (character < 'a' || character > 'z') {
            System.out.println("Character must be between 'a' and 'z'");
            return false;
        }

        return true;
    }
}
