package task6;

import shared.SDANumber;

import java.util.Scanner;

class MainTask6 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a positive number: ");
        var sdaNumber = new SDANumber(scanner.nextInt());
        System.out.println(String.format("Harmonic series: %s", sdaNumber.calculateHarmonicSeries()));
    }
}
