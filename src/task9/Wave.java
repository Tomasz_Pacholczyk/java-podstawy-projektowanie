package task9;

class Wave {
    private final String line_1 = "*      *";
    private final String line_2 = " *    * ";
    private final String line_3 = "  *  *  ";
    private final String line_4 = "   **   ";
    private final String[] lines = {line_1, line_2, line_3, line_4};

    private final int length;

    Wave(int length) {
        this.length = length;
    }

    void print() {
        for (int i = 0; i < 4; i++) { //Mamy 4 linijki
            for (int j = 0; j < length; j++) {
                System.out.print(lines[i]); //drukujemy N razy daną linijkę
            }
            System.out.println(); //jak już wydrukujemy daną linijkę N razy, to wtedy drukujemy ENTER, więc drukujemy kolejną linijkę pod spodem.
        }
    }
}
