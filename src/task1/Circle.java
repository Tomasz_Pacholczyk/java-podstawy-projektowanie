package task1;

class Circle {

    private final double diameter;

    Circle(double diameter) {
        this.diameter = diameter;
    }

    double calculateCircuit() {
        return Math.PI * diameter;
    }
}
