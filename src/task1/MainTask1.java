package task1;

import java.util.Scanner;

class MainTask1 {

    public static void main(String[] args) {
        System.out.print("Enter the diameter: ");
        var scanner = new Scanner(System.in);
        var diameter = scanner.nextDouble();
        var circle = new Circle(diameter);
        System.out.println(String.format("Circuit: %s", circle.calculateCircuit()));
    }
}
