package task8;

class SimpleCalculator {

    private final float firstNumber;
    private final String operation;
    private final float secondNumber;

    SimpleCalculator(float firstNumber, String operation, float secondNumber) {
        this.firstNumber = firstNumber;
        this.operation = operation;
        this.secondNumber = secondNumber;
    }

    void printResult() {
        var result = calculateResult();

        if (result == Float.MIN_VALUE) {
            System.out.println("Cannot divide by 0!");
            return;
        }

        System.out.println(String.format("%s %s %s = %s", firstNumber, operation, secondNumber, result));
    }

    private float calculateResult() {
        return switch (operation) {
            case "/" -> handleDivision(firstNumber, secondNumber);
            case "*" -> firstNumber * secondNumber;
            case "-" -> firstNumber - secondNumber;
            case "+" -> firstNumber + secondNumber;
            default -> Float.MIN_VALUE;
        };
    }

    private static float handleDivision(float firstNumber, float secondNumber) {
        if (secondNumber == 0) {
            return Float.MIN_VALUE;
            //throw new IllegalArgumentException("Cannot divide by 0!");
        }
        return firstNumber / secondNumber;
    }
}
