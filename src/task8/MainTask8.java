package task8;

import java.util.Scanner;

class MainTask8 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter the first number: ");
        float firstNumber = scanner.nextFloat();

        System.out.print("Enter the operation: ");
        String operation = scanner.next();

        if (isOperationNotSupported(operation)) {
            System.out.println("Operation not supported");
            return;
        }

        System.out.print("Enter the second number: ");
        float secondNumber = scanner.nextFloat();

        var simpleCalculator = new SimpleCalculator(firstNumber, operation, secondNumber);
        simpleCalculator.printResult();
    }

    private static boolean isOperationNotSupported(String operation) {
        return !(operation.equals("/")
                || operation.equals("*")
                || operation.equals("-")
                || operation.equals("+"));
    }
}
