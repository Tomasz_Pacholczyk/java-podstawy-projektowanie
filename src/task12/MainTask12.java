package task12;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class MainTask12 {

    public static void main(String[] args) throws IOException {
        var bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter a text: ");
        String inputText = bufferedReader.readLine();

        if (inputText.isEmpty()) {
            System.out.println("Text was not provided");
            return;
        }

        int spaceCounter = 0;

        for (int i = 0; i < inputText.length(); i++) {
            if (inputText.charAt(i) == ' ') {
                spaceCounter++;
            }
        }

        var spacesPercentage = ((float) spaceCounter / inputText.length()) * 100;

        System.out.println(String.format("Spaces par was: %s%%", spacesPercentage));
    }
}
